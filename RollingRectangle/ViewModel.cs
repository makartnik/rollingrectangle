﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace RollingRectangle
{
    public class ViewModel
    {
        public Command StartCommand { get; }
        public ObservableCollection<Point> Rectangle { get; }
        public ObservableCollection<Point> Line { get; }

        public ViewModel()
        {
            Rectangle = new ObservableCollection<Point>(Enumerable.Repeat<Point>(default, 4));
            InitializeRectangle();

            const int iterations = 2;

            Line = new ObservableCollection<Point>
            {
                Rectangle[1],
                new Point(Rectangle[1].X + iterations * 240 + 40, Rectangle[1].Y + iterations * 180 + 30),
            };

            StartCommand = new Command(async o =>
            {
                try
                {
                    StartCommand!.SetExecutable(false);

                    InitializeRectangle();
                    var zeroIndex = 2;

                    for (var i = 0; i < iterations; i++)
                    {
                        for (var j = 0; j < Rectangle.Count; j++)
                        {
                            await Task.Delay(300);
                            if (j == 2)
                            {
                                await Task.Delay(1000);
                            }

                            var zero = Rectangle[zeroIndex];
                            var matrix = new Matrix(0, 1, -1, 0, zero.X + zero.Y, zero.Y - zero.X);
                            for (var k = 0; k < Rectangle.Count; k++)
                            {
                                Rectangle[k] = Point.Multiply(Rectangle[k], matrix);
                            }

                            zeroIndex = (zeroIndex + 1) % Rectangle.Count;
                        }
                    }
                }
                finally
                {
                    StartCommand!.SetExecutable(true);
                }
            });
        }

        private void InitializeRectangle()
        {
            Rectangle[0] = new Point(60, 0);
            Rectangle[1] = new Point(0, 80);
            Rectangle[2] = new Point(40, 110);
            Rectangle[3] = new Point(100, 30);
        }
    }
}

﻿using System;
using System.Windows.Input;

namespace RollingRectangle
{
    public class Command : ICommand
    {
        private readonly Action<object?> _execute;

        public event EventHandler? CanExecuteChanged;

        private bool _canExecute = true;

        public Command(Action<object?> exectute)
        {
            _execute = exectute;
        }

        public bool CanExecute(object? parameter)
        {
            return _canExecute;
        }

        public void Execute(object? parameter)
        {
            _execute(parameter);
        }

        public void SetExecutable(bool value)
        {
            _canExecute = value;
            CanExecuteChanged?.Invoke(this, EventArgs.Empty);
        }
    }
}
